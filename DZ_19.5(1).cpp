#include <iostream>

class Animal
{
public:
    virtual void Voice() const=0;

};
class Cat : public Animal
{
public:
    void Voice() const override
    {
       std::cout << "Make voice:\n";
       
        std::cout << "Meow\n";
    }
};
class Dog : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Make voice:\n";
        std::cout << "Voof\n";
    }
};
class Pig : public Animal
{
public:
    void Voice() const override
    {
        std::cout << "Make voice:\n";
        std::cout << "Oink-oink\n";
    }
};
int main()
{
    Animal* AnimalsArray[3];
    AnimalsArray[0] = new Cat();
    AnimalsArray[1] = new Dog();
    AnimalsArray[2] = new Pig();

    for (Animal* x : AnimalsArray)
        x->Voice();
}
